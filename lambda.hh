#ifndef LAMBDA_LAMBDA_HH_1550420452046996637_
#define LAMBDA_LAMBDA_HH_1550420452046996637_

// clang-format off
#define LAMBDA_MACRO_SELECT_NTH(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16, N, ...) N
#define LAMBDA_MACRO_FOR_EACH0(func)
#define LAMBDA_MACRO_FOR_EACH1(func, _1) func(_1)
#define LAMBDA_MACRO_FOR_EACH2(func, _1, _2) func(_1), func(_2)
#define LAMBDA_MACRO_FOR_EACH3(func, _1, _2, _3) func(_1), func(_2), func(_3)
#define LAMBDA_MACRO_FOR_EACH4(func, _1, _2, _3, _4) func(_1), func(_2), func(_3), func(_4)
#define LAMBDA_MACRO_FOR_EACH5(func, _1, _2, _3, _4, _5) func(_1), func(_2), func(_3), func(_4), func(_5)
#define LAMBDA_MACRO_FOR_EACH6(func, _1, _2, _3, _4, _5, _6) func(_1), func(_2), func(_3), func(_4), func(_5), func(_6)
#define LAMBDA_MACRO_FOR_EACH7(func, _1, _2, _3, _4, _5, _6, _7) func(_1), func(_2), func(_3), func(_4), func(_5), func(_6), func(_7)
#define LAMBDA_MACRO_FOR_EACH8(func, _1, _2, _3, _4, _5, _6, _7, _8) func(_1), func(_2), func(_3), func(_4), func(_5), func(_6), func(_7), func(_8)
#define LAMBDA_MACRO_FOR_EACH9(func, _1, _2, _3, _4, _5, _6, _7, _8, _9) func(_1), func(_2), func(_3), func(_4), func(_5), func(_6), func(_7), func(_8), func(_9)
#define LAMBDA_MACRO_FOR_EACH10(func, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10) func(_1), func(_2), func(_3), func(_4), func(_5), func(_6), func(_7), func(_8), func(_9), func(_10)
#define LAMBDA_MACRO_FOR_EACH11(func, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11) func(_1), func(_2), func(_3), func(_4), func(_5), func(_6), func(_7), func(_8), func(_9), func(_10), func(_11)
#define LAMBDA_MACRO_FOR_EACH12(func, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12) func(_1), func(_2), func(_3), func(_4), func(_5), func(_6), func(_7), func(_8), func(_9), func(_10), func(_11), func(_12)
#define LAMBDA_MACRO_FOR_EACH13(func, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13) func(_1), func(_2), func(_3), func(_4), func(_5), func(_6), func(_7), func(_8), func(_9), func(_10), func(_11), func(_12), func(_13)
#define LAMBDA_MACRO_FOR_EACH14(func, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14) func(_1), func(_2), func(_3), func(_4), func(_5), func(_6), func(_7), func(_8), func(_9), func(_10), func(_11), func(_12), func(_13), func(_14)
#define LAMBDA_MACRO_FOR_EACH15(func, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15) func(_1), func(_2), func(_3), func(_4), func(_5), func(_6), func(_7), func(_8), func(_9), func(_10), func(_11), func(_12), func(_13), func(_14), func(_15)

#define LAMBDA_MACRO_FOR_EACH(...) LAMBDA_MACRO_SELECT_NTH(__VA_ARGS__, LAMBDA_MACRO_FOR_EACH15, LAMBDA_MACRO_FOR_EACH14, LAMBDA_MACRO_FOR_EACH13, LAMBDA_MACRO_FOR_EACH12, LAMBDA_MACRO_FOR_EACH11, LAMBDA_MACRO_FOR_EACH10, LAMBDA_MACRO_FOR_EACH9, LAMBDA_MACRO_FOR_EACH8, LAMBDA_MACRO_FOR_EACH7, LAMBDA_MACRO_FOR_EACH6, LAMBDA_MACRO_FOR_EACH5, LAMBDA_MACRO_FOR_EACH4, LAMBDA_MACRO_FOR_EACH3, LAMBDA_MACRO_FOR_EACH2, LAMBDA_MACRO_FOR_EACH1, LAMBDA_MACRO_FOR_EACH0)(__VA_ARGS__)
// clang-format on


#define LAMBDA_AUTO(x) auto&& x
#define LAMBDA_RETURNS(...)                                                    \
  noexcept(noexcept(__VA_ARGS__))->decltype(__VA_ARGS__) {                     \
    return std::forward<decltype(__VA_ARGS__)>((__VA_ARGS__));                 \
  }


// Detect __VA_OPT__ (https://stackoverflow.com/a/48045656/4990485)
#define LAMBDA_MACRO_THIRD_ARG(a, b, c, ...) c
#define LAMBDA_MACRO_VA_OPT_SUPPORTED_I(...)                                   \
  LAMBDA_MACRO_THIRD_ARG(__VA_OPT__(, ), true, false, )
#define LAMBDA_MACRO_VA_OPT_SUPPORTED LAMBDA_MACRO_VA_OPT_SUPPORTED_I(?)

#if LAMBDA_MACRO_VA_OPT_SUPPORTED
#  define SHORT_LAMBDA(...)                                                    \
    [&](LAMBDA_MACRO_FOR_EACH(LAMBDA_AUTO __VA_OPT__(, ) __VA_ARGS__))         \
        LAMBDA_RETURNS
#else
#  define SHORT_LAMBDA(...)                                                    \
    [&](LAMBDA_MACRO_FOR_EACH(LAMBDA_AUTO, __VA_ARGS__)) LAMBDA_RETURNS
#endif

#ifndef DISABLE_SHORT_LAMBDA_NAME
#  define LL SHORT_LAMBDA
#endif

#endif // LAMBDA_LAMBDA_HH_1550420452046996637_
