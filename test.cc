#include "lambda.hh"

#include <utility>

#include <cassert>

#undef NDEBUG

struct tester {
  enum class constructor { default_, copy, move } constructed_by;
  tester() = default;
  tester(const tester&) { constructed_by = constructor::copy; }
  tester(tester&&) { constructed_by = constructor::move; }
};

int main() {
  tester t;
  auto id = LL(x)(x);

  tester t2(id(t));
  assert(t2.constructed_by == tester::constructor::copy);

  tester t3(id(std::move(t)));
  assert(t3.constructed_by == tester::constructor::move);

  int n = 0;
  ++id(n);
  assert(n == 1);
}
