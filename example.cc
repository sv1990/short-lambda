#include "lambda.hh"

#include <range/v3/view/filter.hpp>
#include <range/v3/view/iota.hpp>
#include <range/v3/view/transform.hpp>
#include <range/v3/view/zip_with.hpp>

#include <iostream>

int main() {
  namespace views = ranges::views;

  auto rng = views::zip_with(LL(x, y)(x + 2 * y), //
                             views::iota(1, 10),  //
                             views::iota(1, 10))  //
             | views::transform(LL(x)(x * x))     //
             | views::filter(LL(x)(x <= 50));

  std::cout << rng << '\n';
}
