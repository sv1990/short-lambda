# Short Lambda

This small library defines a macro `LL` that can be used to shorten the definition of lambdas.

Instead of writing

```c++
auto f = [](auto x, auto y) {return x > y;};
```

one may write

```c++
auto f = LL(x, y)(x > y);
```

which is then expanded to 

```c++
auto f = [&](auto&& x, auto&& y) noexcept(noexcept(x > y)) -> decltype(x > y) { return std::forward<decltype(x > y)>((x > y)); };
```

`LL` supports from zero up to fifteen parameters. For zero parameters `__VA_OPT__` support is needed.

Due to the semantics of `decltype` the function `f` will return references which can lead to dangling references.